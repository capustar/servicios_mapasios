//
//  Pin.swift
//  Mapas
//
//  Created by alumno on 15/5/18.
//
//

import UIKit
import MapKit

class Pin: NSObject, MKAnnotation
{
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var thumbImage: UIImage
    
    init(num: Int, coordinate: CLLocationCoordinate2D)
    {
        self.title = "Pin \(num)"
        self.subtitle = "Un hermoso lugar"
        self.coordinate = coordinate
        
        if (num % 2 == 0)
        {
            self.thumbImage = UIImage(named: "pablo_iglesias.jpg")!
        } else {
            self.thumbImage = UIImage(named: "albert_rivera.jpg")!
        }
        
        super.init()
    }

}
