//
//  ViewController.swift
//  Mapas
//
//  Created by alumno on 15/5/18.
//
//

import UIKit
import MapKit

class ViewController: UIViewController, MKMapViewDelegate
{
    @IBOutlet weak var segmented_control_tipo_mapa: UISegmentedControl!
    
    var numero_anotaciones_pin : Int!
    
    @IBOutlet weak var map_view: MKMapView!
    {
        didSet {
            map_view.mapType = .standard
            map_view.delegate = self
            let alicanteLocation =
                CLLocationCoordinate2D(latitude: 38.3453,
                                       longitude: -0.4831)
            let initialLocation =
                CLLocation(latitude: alicanteLocation.latitude,
                           longitude: alicanteLocation.longitude)
            centerMapOnLocation(mapView: map_view, loc: initialLocation)
            // definida en la siguiente diapositiva
        }
    }
    
    // ----------------------------------------------------------------------------------------
    
    func centerMapOnLocation(mapView: MKMapView, loc: CLLocation)
    {
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion =
            MKCoordinateRegionMakeWithDistance(loc.coordinate,
                                               regionRadius * 4.0, regionRadius * 4.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    // ----------------------------------------------------------------------------------------
    
    
    @IBAction func cambiarTipoMapa(_ sender: UISegmentedControl)
    {
        if map_view.mapType == .satellite
        {
            map_view.mapType = .standard
        }
        else if map_view.mapType == .standard
        {
            map_view.mapType = .satellite
        }
        else
        {
            map_view.mapType = .standard
        }
    }
    
    // ----------------------------------------------------------------------------------------

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        if (annotation is MKUserLocation)
        {
            return nil
        }
        else
        {
            let view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: nil)
            view.pinTintColor = UIColor.red
            view.animatesDrop = true
            view.canShowCallout = true
        
            let pin = annotation as! Pin
        
            let thumbnailImageView = UIImageView(frame: CGRect(x:0, y:0, width: 59, height: 59))
            thumbnailImageView.image = pin.thumbImage
            view.leftCalloutAccessoryView = thumbnailImageView
        
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure) as UIView
        
            return view
        }
    }
    
    // ----------------------------------------------------------------------------------------

    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl)
    {
        performSegue(withIdentifier: "DetalleImagen", sender: view)
    }
    
    // ----------------------------------------------------------------------------------------

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "DetalleImagen"
        {
            if let pin = (sender as? MKAnnotationView)?.annotation as? Pin
            {
                if let vc = segue.destination as? ImageDetailViewController
                {
                    vc.imageDetail = pin.thumbImage
                }
            }
        }
    }
    
    // ----------------------------------------------------------------------------------------

    
    @IBAction func anadirAnotacionMapa(_ sender: UIBarButtonItem)
    {
        let pin = Pin(num: numero_anotaciones_pin, coordinate: map_view.centerCoordinate)
        map_view.addAnnotation(pin)
        numero_anotaciones_pin = numero_anotaciones_pin + 1;
    }
    
    // ----------------------------------------------------------------------------------------

    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!)
    {
        let location = locations.last as! CLLocation
        
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        self.map_view.setRegion(region, animated: true)
    }

    // ----------------------------------------------------------------------------------------
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        numero_anotaciones_pin = 0;
        
        let userTrackingButton = MKUserTrackingBarButtonItem(mapView: map_view)
        self.navigationItem.leftBarButtonItem = userTrackingButton
    }

    // ----------------------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

