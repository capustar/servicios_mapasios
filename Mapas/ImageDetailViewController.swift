//
//  ImageDetailViewController.swift
//  Mapas
//
//  Created by alumno on 15/5/18.
//
//

import UIKit

class ImageDetailViewController: UIViewController
{

    @IBOutlet weak var imageView: UIImageView!
    var imageDetail: UIImage? = nil
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        if imageDetail != nil
        {
            imageView.image = imageDetail!
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
